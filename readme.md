SimpleWeather documentation
================
# Notes

- This is a very basic example of a weather app that uses ReactiveCocoa to fetch weather data from the [Openweathermap.org API](https://openweathermap.org/api) and displays hourly Predictions by scrolling up.
The original version of this app was written in Objective-C for iOS 7 and can be found [here](https://www.raywenderlich.com/2579-ios-7-best-practices-a-weather-app-case-study-part-1-2)  
this is an update of that project in Swift 4 and using new design patterns 

## Things considered
- Keeping it brief I used a simple use of a ViewModel object to display on the View Controller
With more time These considerations

- Separate Coordinator to control creation of Detail View Controllers
- Prefetching on tableviews to create a smoother experience and less memory
- Extensive Unit Testing on components
- UI Tests for Intergration
- Threading consideration for fetching data when location changes
- Error Cases ie Network issues ,missing data
- Adaptive Layout coverage
- POP(Protocol Oriented Programing) use to mock objects like the WXManager and WXClient during testing

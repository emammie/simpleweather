//
//  WXManager.swift
//  SimpleWeatherUniversal
//
//  Created by Eugene Mammie on 12/20/18.
//  Copyright © 2018 Eugene Mammie. All rights reserved.
//

import Foundation


import UIKit
import CoreLocation
import ReactiveSwift
import RMessage
import Result

class WXManager: NSObject {
    
    let currentLocation = MutableProperty<CLLocation?>(nil)
    var currentCondition = MutableProperty<WXCondition?>(nil)
    var hourlyForcast = MutableProperty<[WXHourlyCondition]>([WXHourlyCondition]())
    var dailyForcast = MutableProperty<[WXDailyCondition]>([WXDailyCondition]())
    
    let locationManager = CLLocationManager()
    
    var client : WXClient = WXClient()
    var isFirstUpdate = true
    
    static let sharedInstance = { () -> WXManager in
        let instance = WXManager()
        return instance
    }
    
    override init() {
        super.init()
        locationManager.delegate = self
        locationManager.requestWhenInUseAuthorization()
        locationManager.requestAlwaysAuthorization()
        locationManager.distanceFilter = 10
        
        let observer = Signal<CLLocation?,NoError>.Observer{ (event) in
            switch event {
            case let .value(v):
                print("Test Observation value = \(v)")
            case let .failed(error):
                print("Test Observation error = \(error)")
            case .completed:
                print("Test Observation completed")
            case .interrupted:
                print("Test Observation interrupted")
            }
        }
        currentLocation.signal.observe(observer)
        
        currentLocation.signal.observeValues { location in
            
            self.updateCurrentConditions().observe(){ event in
                
            }
            self.updateHourlyConditions().observe() { event in
                
            }
            /* let aSignal =  Signal<[Signal],NoError>.merge(signal[self.updateCurrentConditions(),self.updateHourlyConditions()]).observe(on: UIScheduler())*/
        }
    }
    
    func findCurrentLocation(){
        isFirstUpdate = true
        locationManager.startUpdatingLocation()
    }
    
    func updateCurrentConditions() ->Signal<WXCondition,WXClientError> {
        guard let locale = currentLocation.value else {
            let spot = CLLocationCoordinate2D(latitude: 0.0, longitude: 0.0)
            return client.fetchCurrentConditionsForLocation(coordinate: spot)
        }
        return client.fetchCurrentConditionsForLocation(coordinate: locale.coordinate).on(event: { event in
            print("Event in updateCurrentConditions\(event)")
        },
                                                                                          failed: { event in
                                                                                            print("Failed in updateCurrentConditions \(event)")
        }, value : { condition in
            self.currentCondition.value = condition
        })
    }
    
    func updateHourlyConditions() ->Signal<[WXHourlyCondition],WXClientError> {
        guard let locale = currentLocation.value else {
            let spot = CLLocationCoordinate2D(latitude: 0.0, longitude: 0.0)
            return client.fetchHourlyForecastForLocation(coordinate: spot)
        }
        return client.fetchHourlyForecastForLocation(coordinate: locale.coordinate).on(event: { event in
            print("Event in updateHourlyConditions\(event)")
        },
                                                                                       failed: { event in
                                                                                        print("Failed in updateHourlyConditions \(event)")
        }, value : { conditions in
            self.hourlyForcast.value = conditions
        })
    }
}

    extension WXManager : CLLocationManagerDelegate {
    // Location Manager Delegate Methods
    func locationManager(_ manager: CLLocationManager, didUpdateLocations locations: [CLLocation]) {
        
        if (isFirstUpdate) {
            isFirstUpdate = false
            return
        }
        
        if let location = locations.last{
            if (location.horizontalAccuracy > 0 ){
                self.currentLocation.value = location
                print("Location Info: \(location)")
                //locationManager.stopUpdatingLocation()
            }
        }
    }
    
    func  locationManager(_ manager: CLLocationManager, didFailWithError error: Error) {
        //handle Errors
    }
}

//
//  WXClient.swift
//  SimpleWeatherUniversal
//
//  Created by Eugene Mammie on 12/19/18.
//  Copyright © 2018 Eugene Mammie. All rights reserved.


import UIKit
import CoreLocation
import ReactiveSwift
import ReactiveCocoa
import Result

enum WXClientError : Error {
    case commonError
    case webFailure(description:String)
    case parseFailure
}

typealias jsonData = [String : Any]

class WXClient: NSObject {
    
    let APPID = "f9976cb284fb9b6ffa68977af727e5fb"
    var urlComponent = URLComponents(string: "https://api.openweathermap.org")
    
    lazy var session = { () -> URLSession in
        let sessionConfig =    URLSessionConfiguration.default
        let session = URLSession(configuration: sessionConfig)
        return session
    }
    
    func fetchDataFromURL(url:URL) -> SignalProducer<Data,WXClientError> {
        return SignalProducer<Data,WXClientError> { observer, _ in
            let task = self.session().dataTask(with: url) { (data, URLResponse, Error) in
                if let Error = Error {
                    observer.send(error: WXClientError.webFailure(description: Error.localizedDescription))
                } else if let httpResponse = URLResponse as? HTTPURLResponse {
                    if httpResponse.statusCode == 200 {
                        observer.send(value:data!)
                    }
                }
            }
            task.resume()
        }
    }
    
    func fetchCurrentConditionsForLocation(coordinate:CLLocationCoordinate2D) -> Signal<WXCondition,WXClientError> {
        return Signal<WXCondition,WXClientError> { observer , some in
            if let url = URL(string: "http://api.openweathermap.org/data/2.5/weather?lat=\(coordinate.latitude)&lon=\(coordinate.longitude)&units=imperial&APPID=\(APPID)") {
                let signalObserver = Signal<Data, WXClientError>.Observer (
                    value: { value in
                        do{
                            let decoder = JSONDecoder()
                            let currentCondition = try decoder.decode(WXConditionService.self, from: value)
                            observer.send(value:WXCondition(service:currentCondition))
                        }
                        catch
                        {
                            observer.send(error: WXClientError.parseFailure)
                        }
                }, completed: {
                    observer.sendCompleted()
                }, interrupted: {
                })
                fetchDataFromURL(url: url).start(signalObserver)
                }
            }
    }
    
    func fetchHourlyForecastForLocation(coordinate:CLLocationCoordinate2D) -> Signal<[WXHourlyCondition],WXClientError> {
        return Signal<[WXHourlyCondition],WXClientError> { observer ,some in
            if let url = URL(string: "http://api.openweathermap.org/data/2.5/forecast?lat=\(coordinate.latitude)&lon=\(coordinate.longitude)&units=imperial&cnt=40&APPID=\(APPID)") {
                fetchDataFromURL(url: url).start(){ event in
                    if let value = event.value {
                        do{
                            let decoder = JSONDecoder()
                            let val = try decoder.decode(WXHourlyConditionService.self, from: value)
                            let conditions = val.list//val.list.map(){ return WXHourlyCondition(service:$0)}
                            observer.send(value:conditions)
                        }
                        catch
                        {
                            observer.send(error:  WXClientError.parseFailure)
                        }
                    }
                }
            }
        }
    }
    
    /* premium API Key needed
     func fetchDailyForecastForLocation(coordinate:CLLocationCoordinate2D) -> SignalProducer<Any,NoError> {
     
     let url = URL(string: "http://api.openweathermap.org/data/2.5/forecast/daily?lat=\(coordinate.latitude)&lon=\(coordinate.longitude)&units=imperial&cnt=7&appid=\(APPID)")
     return fetchJSONFromURL(url: url!).map({ data in
     var jSONDict = data as? [AnyHashable:Any]
     let dailyForcasts = jSONDict?["list"] as! [Any]
     
     return dailyForcasts.map({
     item -> Any in
     let oneDay = item as! [AnyHashable:Any]
     do{
     // let val = try MTLJSONAdapter.model(of: WXDailyForcast.self, fromJSONDictionary: oneDay)
     //  return val
     }
     catch
     {
     print("Error ------ \(error)")
     return data
     }
     })
     
     })
     
     }
     */
    
}

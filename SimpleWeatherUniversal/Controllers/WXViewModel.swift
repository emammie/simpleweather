//
//  WXViewModel.swift
//  SimpleWeatherUniversal
//
//  Created by Eugene Mammie on 12/21/18.
//  Copyright © 2018 Eugene Mammie. All rights reserved.
//

import Foundation
import ReactiveSwift
import Result

class WXViewModel {
    
    let temperatureString = MutableProperty<String?>(nil)
    let conditionString = MutableProperty<String?>(nil)
    let cityString = MutableProperty<String?>(nil)
    let iconImage = MutableProperty<UIImage?>(nil)
    let (hiloSignal, hiloObserver) = Signal<String,NoError>.pipe()
    let (dailyForcastSignal, dailyForcastObserver) = Signal<[WXDailyCondition],NoError>.pipe()
    let (hourlyForcastSignal, hourlyForcastObserver) = Signal<[WXHourlyCondition],NoError>.pipe()
    var hourlyForcasts = MutableProperty<[WXHourlyCondition]>([WXHourlyCondition]())
    var dailyForcasts = MutableProperty<[WXDailyCondition]>([WXDailyCondition]())
    var currentCondition : WXCondition?
    
    var manager = WXManager.sharedInstance
    
    init(manager : WXManager = WXManager.sharedInstance()) {
        manager.currentCondition.signal.observe(on: UIScheduler()).observeValues{ newCondition in
            self.currentCondition = newCondition
            self.temperatureString.value = String(format:"%.0f" ,(newCondition?.temperature)!)
            self.conditionString.value = newCondition?.condition.capitalized
            self.cityString.value = newCondition?.locationName.capitalized
            self.iconImage.value = UIImage(named: (newCondition?.imageNamed())!)
        }
    
        manager.currentCondition.signal.skipNil().observe(on:UIScheduler()).observeValues { condition in
            self.hiloObserver.send(value:String(format: "%.0f° / %.0f°", condition.tempHigh,condition.tempLow))
        }
    
        manager.dailyForcast.signal.observe(on:UIScheduler()).observeValues(){ forcasts in
            //self.dailyForcasts.value = forcasts
        }
        manager.hourlyForcast.signal.observe(on:UIScheduler()).observeValues(){ forcasts in
            self.hourlyForcasts.value = forcasts
        }
        
        manager.findCurrentLocation()
    }
}

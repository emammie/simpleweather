//
//  WXViewController.swift
//  SimpleWeatherUniversal
//
//  Created by Eugene Mammie on 12/19/18.
//  Copyright © 2018 Eugene Mammie. All rights reserved.
//

import UIKit
import ReactiveSwift
import ReactiveCocoa
import Result

class WXViewController: UIViewController , UIScrollViewDelegate {
    
    @IBOutlet weak var bgImgView: UIImageView!
    @IBOutlet weak var blurredVisualView: UIVisualEffectView!
    @IBOutlet weak var tableView: UITableView!
    @IBOutlet weak var tableViewHeaderView: UIView!
    
    @IBOutlet weak var temperatureLabel: UILabel!
    @IBOutlet weak var hiloLabel: UILabel!
    @IBOutlet weak var cityLabel: UILabel!
    @IBOutlet weak var conditionsLabel: UILabel!
    @IBOutlet weak var iconView: UIImageView!
    
    var screenHeight : CGFloat =  UIScreen.main.bounds.size.height
    var viewModel = WXViewModel(manager:  WXManager.sharedInstance())

    var hourlyFormatter : DateFormatter {
        let dateformatter = DateFormatter()
        dateformatter.dateFormat = "h a"
        return dateformatter
    }
    var dailyFormatter : DateFormatter {
        let dateformatter = DateFormatter()
        dateformatter.dateFormat = "EEEE"
        return dateformatter
    }
    
    override var preferredStatusBarStyle: UIStatusBarStyle { return .lightContent}
    
    //MARK: View Controller LifeCycle
    override func viewDidLoad() {
        super.viewDidLoad()

        self.tableView.delegate = self
        self.tableView.dataSource = self
        self.tableView.separatorColor = UIColor.init(white: 1, alpha: 0.2)
        
        //let headerFrame = UIScreen.main.bounds

        cityLabel.reactive.text <~ viewModel.cityString
        temperatureLabel.reactive.text <~ viewModel.temperatureString
        conditionsLabel.reactive.text <~ viewModel.conditionString
        iconView.reactive.image <~ viewModel.iconImage
        
        hiloLabel.reactive.text <~ viewModel.hiloSignal
        
        viewModel.dailyForcasts.signal.observe(on:UIScheduler()).observeValues(){ forcasts in
            self.tableView.reloadData()
        }
        viewModel.hourlyForcasts.signal.observe(on:UIScheduler()).observeValues(){ forcasts in
            self.tableView.reloadData()
        }
       
    }
    
    override func viewWillLayoutSubviews() {
        let bounds = self.view.bounds
        bgImgView.frame = bounds
        blurredVisualView.frame = bounds
        tableView.frame = bounds
    }
}

//MARK: TableView Delegate & DataSource Methods
extension WXViewController : UITableViewDataSource, UITableViewDelegate {

    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
            if (section == 0) {
                return min(viewModel.hourlyForcasts.value.count, 6) + 1
            }
            return min(viewModel.dailyForcasts.value.count, 6) + 1
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cellIdentifier = "CellIdentifier"
        guard let cell = tableView.dequeueReusableCell(withIdentifier: cellIdentifier) else {
            return UITableViewCell(style: UITableViewCell.CellStyle.value1, reuseIdentifier: cellIdentifier)
        }
       
        cell.backgroundColor = UIColor(white: 0, alpha: 0.2)
        cell.selectionStyle = .none
    
        if (indexPath.section == 0) {
            if (indexPath.row == 0) {
                self.configureHeaderCell(cell , title:"Hourly Forecast")
            }
            else {
                let weather = viewModel.hourlyForcasts.value[indexPath.row - 1]
                self.configureHourlyCell(cell, weather: weather)
            }
        }
        else if (indexPath.section == 1) {
            if (indexPath.row == 0) {
                self.configureHeaderCell(cell, title:"Daily Forecast")
            }
            else {
                let weather = viewModel.dailyForcasts.value[indexPath.row - 1]
                self.configureDailyCell(cell ,weather:weather)
            }
        }
        return cell
    }

    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        let cellCount = self.tableView.numberOfRows(inSection: indexPath.section)
        return self.screenHeight / CGFloat(cellCount)
    }
    
    func configureHeaderCell(_ cell : UITableViewCell ,title: String) {
        cell.textLabel?.font = UIFont.init(name:"HelveticaNeue-Medium", size:18)
        cell.textLabel?.text = title
        cell.textLabel?.textColor = .white
        cell.detailTextLabel?.text = ""
        cell.imageView?.image = nil
        cell.detailTextLabel?.isHidden = true
    }
    
    func configureHourlyCell(_ cell: UITableViewCell, weather: WXHourlyCondition){
        cell.textLabel?.font = UIFont.init(name:"HelveticaNeue-Light", size:18)
        cell.textLabel?.textColor = .white
        cell.detailTextLabel?.font = UIFont.init(name:"HelveticaNeue-Medium", size:18)
        cell.detailTextLabel?.textColor = .white
        cell.textLabel?.text = self.hourlyFormatter.string(from:Date(timeIntervalSince1970: weather.dt))
        cell.detailTextLabel?.text = String.init(format:"%.0f°",weather.main.temp)
        cell.imageView?.image = UIImage(named: weather.imageNamed())
        cell.imageView?.contentMode = .scaleAspectFit
    }
    
    func configureDailyCell(_ cell: UITableViewCell, weather: WXDailyCondition ){
        cell.detailTextLabel?.font = UIFont.init(name:"HelveticaNeue-Light", size:18)
        cell.detailTextLabel?.font = UIFont.init(name:"HelveticaNeue-Medium", size:18)
        let date = self.dailyFormatter.date(from: weather.time)
        cell.textLabel?.text = self.dailyFormatter.string(from:date!)
        cell.detailTextLabel?.text = String(format: "%.0f° / %.0f°",weather.temp.max,weather.temp.min)
        cell.imageView?.image = UIImage(named: weather.imageNamed())
        cell.imageView?.contentMode = .scaleAspectFit
    }
    
    func scrollViewDidScroll(_ scrollView: UIScrollView) {
        let height = scrollView.bounds.size.height
        let position = max(scrollView.contentOffset.y, 0.0)
        
         let percent = min(position / height, 1.0)
    
        self.blurredVisualView.alpha = percent
    }
}
